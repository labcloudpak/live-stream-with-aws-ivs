<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <script src="https://player.live-video.net/1.3.1/amazon-ivs-player.min.js"></script>
        <style>
            /* video {
    height: 100%;
    width: 100%;
    left: 0;
    top: 0;
    position: fixed;
}



video::-webkit-media-controls-timeline,
video::-webkit-media-controls-current-time-display {
    display: none;
} */

        </style>
    </head>
    <body>

     <div class="container">
        <h2 class="text-info">IVS STREAM DEMO</h2>
         <div class="row">
             <div class="col-8">
                 <video id="video-player" controls playsinline></video>  
             </div>
             <div class="col-4">
                 
             </div>
         </div>
    </div>   
    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    
    <script>
        
// This shows how to include the Amazon IVS Player with a script tag from our CDN
// If self hosting, you may not be able to use the create() method since it requires
// that file names do not change and are all hosted from the same directory.

(function (IVSPlayerPackage) {
    // First, check if the browser supports the IVS player.
    if (!IVSPlayerPackage.isPlayerSupported) {
        console.warn("The current browser does not support the IVS player.");
        return;
    }

    const PlayerState = IVSPlayerPackage.PlayerState;
    const PlayerEventType = IVSPlayerPackage.PlayerEventType;

    // Initialize player
    const player = IVSPlayerPackage.create();
    console.log("IVS Player version:", player.getVersion());
    player.attachHTMLVideoElement(document.getElementById("video-player"));
        // Attach event listeners
    player.addEventListener(PlayerState.PLAYING, function () {
        // console.log("Player State - PLAYING");
        alert('Player State - PLAYING');
    });
    player.addEventListener(PlayerState.ENDED, function () {
        // console.log("Player State - ENDED");
        alert("Player State - ENDED");
    });
    player.addEventListener(PlayerState.READY, function () {
        // console.log("Player State - READY");
        alert("Player State - READY");
    });
    player.addEventListener(PlayerEventType.ERROR, function (err) {
        console.warn("Player Event - ERROR:", err);
    });
    player.addEventListener(PlayerEventType.TEXT_METADATA_CUE, (cue) => {
        const metadataText = cue.text;
        const position = player.getPosition().toFixed(2);
        console.log(
            `PlayerEvent - TEXT_METADATA_CUE: "${metadataText}". Observed ${position}s after playback started.`
        );
    });

    // Setup stream and play
    player.setAutoplay(true);
    player.load(
        // add playback configuration here
        "https://687539d67a2e.eu-west-1.playback.live-video.net/api/video/v1/eu-west-1.405446070454.channel.NvdtiP9WeASl.m3u8"
    );
    
    player.setVolume(0.5);
    
    })(window.IVSPlayer);

    </script>

    </body>
</html>
